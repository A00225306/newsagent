import java.util.List;

class DeliveryDocket {

	public boolean createDelDock(int driverID, Database db) {
		boolean res = false;
		try {
			if (driverID < 1 || driverID > 1000) {
				throw new ExceptionHandler("Invalid Driver ID");
			} else if (db.createDelDock(driverID)) {
				res = true;
			} else
				res = false;
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
		}
		return res;
	}

	public String lookUpDelDock(int delDockID, Database db) {
		if (delDockID < 1) {
			return "Invalid Delivery Area ID";
		} else {
			String res = db.lookUpDelDock(delDockID);
			System.out.println(res);
			return res;
		}
	}

	public String addSubToDelDock(int delDockID, int subID, Database db) {
		if (delDockID < 1) {
			return "Invalid Delivery Docket ID";
		} else if (subID < 1) {
			return "Invalid Subscription ID";
		} else {
			String res = db.addSubToDelDock(delDockID, subID);
			System.out.println(res);
			return res;
		}
	}
	public String archiveDelDock(int delDockID, Database db) {
		String res = "";
		try {
			if (delDockID < 1) {
				throw new ExceptionHandler("Invalid Docket ID");
			} else if (db.lookUpDelDock(delDockID).equals("Delivery Docket Not Found")) {
				res = "Delivery Docket Not Found";
			} else {
				res = db.archiveDelDock(delDockID);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res;
	}

	public boolean modifyDeliveryDocket(int driverID,int delDockID ,Database db) {
			boolean res = false;
			try {
				if (driverID < 1 ||  driverID > 99) {
					throw new ExceptionHandler("Invalid ID");
					}
				 else if (db.modifyDocket(driverID,delDockID)){ 
					res = true;
				} 
			} catch (ExceptionHandler e) {
				System.out.println(e.getMessage());
			}
			return res;
	}
	
	public String completeDeliveryDocket(int delDockID, boolean status, Database db) {
		String res = "";
		try {
			if (delDockID<1){ 
				throw new ExceptionHandler("Invalid Docket ID");
			} else {
				res = db.completeDeliveryDocket(delDockID, status);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res;
}

}
