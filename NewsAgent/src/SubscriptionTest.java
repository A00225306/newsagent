import junit.framework.TestCase;

public class SubscriptionTest extends TestCase {
	Database db = new Database();
	
	/*
	 * Test Number 1
	 * Test Objective: Test  valid freqency
	 * Input:   frequency Daily
	 * Expected Output = true
	 */
	
	
	public void testvalidateCustomerSubscription001() {
		db.initDBConnection();
		Subscription testObj = new Subscription();
		assertEquals(true, testObj.createSubscritpion("Daily",db));
	}

	/*
	 * Test Number 2
	 * Test Objective: test valid freq
	 * Input: freq "MtoF"
	 * Expected Output = true
	 */

	public void testvalidateCustomerSubscription002() {
		db.initDBConnection();
		Subscription testObj = new Subscription();
		assertEquals(true, testObj.createSubscritpion("MtoF",db));
	}
	
	/*
	 * Test Number 4
	 * Test Objective: invalid freq
	 * Input: "abcdef"
	 * Expected Output = false
	 */

	public void testvalidateCustomerSubscription003() {
		db.initDBConnection();
		Subscription testObj = new Subscription();
		assertEquals(true, testObj.createSubscritpion("Weekend",db));
	}
	
	/*
	 * Test Number 4
	 * Test Objective: Test  valid freq
	 * Input:"abcdef"
	 * Expected Output = false
	 */

	public void  testvalidateCustomerSubscription004() {
		Subscription testObj = new Subscription();
		assertEquals(false, testObj.createSubscritpion("abcdef",db));
	}
	
	/*
	 * Test Number 5
	 * Test Objective: test valid addPubToSub
	 * Input: 1, 1
	 * Expected Output = "Publication ID 1 added to Subscription ID 1"
	 */

	public void  testValidAddPubToSub005() {
		db.initDBConnection();
		Subscription testObj = new Subscription();
		assertEquals("Publication ID 1 added to Subscription ID 1", testObj.addPubToSub(1,1,db));
	}
	
	/*
	 * Test Number 6
	 * Test Objective: test invalid addPubToSub
	 * Input: -1, 1
	 * Expected Output = "Invalid Sub ID"
	 */

	public void  testInalidAddPubToSub006() {
		Subscription testObj = new Subscription();
		assertEquals("Invalid Subscription ID", testObj.addPubToSub(-1,1,db));
	}
	
	/*
	 * Test Number 7
	 * Test Objective: test invalid addPubToSub
	 * Input: 1, -1
	 * Expected Output = "Invalid Pub ID"
	 */

	public void  testInalidAddPubToSub007() {
		Subscription testObj = new Subscription();
		assertEquals("Invalid Publication ID", testObj.addPubToSub(1,-1,db));
	}
	
	/*
	 * Test No.1 
	 * TestObjective: Test valid modify Subscription frequency. 
	 * Input(s): Daily
	 * Expected Output(s): true
	 */
	public void testValidModifySubFreq001() {
		db.initDBConnection();
		Subscription tObject = new Subscription();
		assertEquals(true,tObject.modifySubscription(1, "Daily", true, db));
	}



	/*
	 * Test No.2 TestObjective: Test Invalid modify Subscription
	 * frequency. Input(s): Yearly Expected Output(s): false
=======
	
>>>>>>> branch 'master' of https://A00197813@bitbucket.org/A00225306/newsagent.git
>>>>>>> branch 'master' of https://dcoss23@bitbucket.org/A00225306/newsagent.git
=======
>>>>>>> branch 'master' of https://dcoss23@bitbucket.org/A00225306/newsagent.git
	
	/*
	 * Test No.2 
	 * TestObjective: Test Invalid modify Subscription frequency. 
	 * Input(s): Yearly
	 * Expected Output(s): false
	 */
	public void testInvalidModifySubFreq002() {
		Subscription tObject = new Subscription();
		assertEquals(false,tObject.modifySubscription(1, "Yearly", true, db));
	}
	
	/*
	 * Test No.3
	 * TestObjective: Test valid modify Subscription active. 
	 * Input(s): Daily
	 * Expected Output(s): true
	 */
	public void testValidModifySubActive003() {
		db.initDBConnection();
		Subscription tObject = new Subscription();
		assertEquals(true,tObject.modifySubscription(1, "Daily", false, db));
	}



	/*
	 * Test Number 8 Test Objective: test invalid subscription lookup Input: -1
=======

	/* Test Number 8
=======
	
	/* Test Number 8
>>>>>>> branch 'master' of https://A00197813@bitbucket.org/A00225306/newsagent.git
=======
	/*
>>>>>>> branch 'master' of https://dcoss23@bitbucket.org/A00225306/newsagent.git
	 * Test Objective: test invalid subscription lookup
	 * Input: -1
	 * Expected Output = "Invalid Sub ID"
	 */
	public void testInvalidlookUpSub001() {
		Subscription testObj = new Subscription();
		assertEquals("Invalid Subscription ID", testObj.subLookUp(-1, db));
	}

	/*
	 * Test Number 9 Test Objective: test valid subscription lookup Input: 1
	 * Expected Output = true
	 */
	public void testValidLookUpSub002() {
		Subscription testObj = new Subscription();
		db.initDBConnection();
		String testSub = "CustomerID = 1 PublicationID = 1 Frequency = Daily Delivered  = True";
		assertEquals(testSub, testObj.subLookUp(1, db));

	}

	/*
	 * Test Number 10 Test Objective: Test valid archive Docket Input: CustID =1
	 * Expected Output = "Successfully Archived DeliveryDocket: 1"
	 */public void testValidArchiveSub010() {
		Subscription testObj = new Subscription();
		db.initDBConnection();
		assertEquals("Valid Subscription: 1", testObj.archiveSub(1, db));
	}

	/*
	 * Test Number 11 Test Objective: Test invalid archive DeliveryDocket Input:
	 * CustID = -1 Expected Output = "Invalid Docket ID"
	 */

	public void testValidArchiveSub011() {
		Subscription testObj = new Subscription();
		db.initDBConnection();
		assertEquals("Invalid Subscription ID", testObj.archiveSub(-1, db));
	}

	/*
	 * Test Number 17 Test Objective: Test nto found archive DeliveryDocket
	 * Input: CustID = 999 Expected Output = "Delivery Docket Not Found"
	 */

	public void testValidArchiveSubNotFound012() {
		Subscription testObj = new Subscription();
		db.initDBConnection();
		assertEquals("Subscription  Not Found", testObj.archiveSub(999, db));
	}

	}

