
public class DeliveryArea {
	public boolean createDeliveryArea(int delAreaID, String AreaLocation, Database db) {
		boolean res = false;
		try {
			if (delAreaID < 1 || delAreaID > 100) {
				throw new ExceptionHandler("Invalid Delivery ID");
			} else if (AreaLocation.length() < 14 || AreaLocation.length() > 50) {
				throw new ExceptionHandler("Invalid Delivery Area location");
			} else {
				db.createDelArea(delAreaID, AreaLocation);
				res = true;
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
		}
		return res;
	}

	public String lookupArea(int delAreaID, Database db) {
		String res2 = "";
		try {
			if (delAreaID < 1) {
				throw new ExceptionHandler("Invalid Delivery Area ID");
			} else {
				res2 = db.lookupDelArea(delAreaID);
				System.out.println(res2);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res2 = e.getMessage();
		}
		return res2;
	}
	public String archiveDelArea(int delAreaID, Database db) {
		String res = "";
		try {
			if (delAreaID < 1 || delAreaID > 100) {
				throw new ExceptionHandler("Invalid DeliveryArea ID");
			} else if (db.lookupDelArea(delAreaID).equals("Delivery Area not found")) {
				res = "Delivery Area Not Found";
			} else {
				res = db.archiveDelArea(delAreaID);
				System.out.println("Successfully Archived DeliveryArea ID: " + delAreaID);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res;
	}
	
	public boolean modifyDeliveryArea(int delAreaID, String AreaLocation, Database db) {
		boolean res = false;
		try {
			if (delAreaID < 1 || delAreaID > 100) {
				throw new ExceptionHandler("Invalid Delivery ID");
			} else if (AreaLocation.length() < 14 || AreaLocation.length() > 50) {
				throw new ExceptionHandler("Invalid Delivery Area location");
			} else {
				db.modifyDelArea(delAreaID, AreaLocation);
				res = true;
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
		}
		return res;
	}
	
	
	
	
	
	
	
	
	
}
