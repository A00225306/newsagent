
public class Driver {

	public boolean createDriver(String DriverName, String Password, Database db) {
		boolean res = false;
		try {
			if (DriverName.matches(".*\\d+.*")) {
				throw new ExceptionHandler("Invalid Driver Name");
			} else if (Password.length() < 7 || Password.length() > 30) {
				throw new ExceptionHandler("Invalid Driver Password");
			} else {
				db.createDriver(DriverName, Password);
				res = true;
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
		}
		return res;
	}

	public String lookupDriver(int DriverId, Database db) {
		String res = "";
		try {
			if (DriverId < 1) {
				throw new ExceptionHandler("Invalid Driver ID");
			} else {
				res = db.lookupDriver(DriverId);
				System.out.println(res);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res;
	}

	public boolean modifyDriver(int DriverId, String DriverName, String Password, Database db) {
		boolean res = false;
		try {

			if (DriverId < 1 || DriverId > 999) {
				throw new ExceptionHandler("Invalid Driver Id");
			}
			if (DriverName.matches(".*\\d+.*")) {
				throw new ExceptionHandler("Invalid Driver Name");
			} else if (Password.length() < 7 || Password.length() > 30) {
				throw new ExceptionHandler("Invalid Driver Password");
			} else if (db.modifyDriver(DriverId, Password, DriverName)){
				res = true;
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
		}
		return res;
	}
	
	
	public String archiveDriver(int DriverId, Database db) {
		String res = "";
		try {
			if (DriverId < 1) {
				throw new ExceptionHandler("Invalid Driver ID");
			} else if (db.lookupDriver(DriverId).equals("Driver Not Found")) {
				res = "Driver Not Found";
			} else {
				res = db.archiveDriver(DriverId);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res;
	}
	
	
	

}
