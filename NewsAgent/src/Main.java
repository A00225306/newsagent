
import java.util.Scanner;

public class Main {
	static Scanner scanner = new Scanner(System.in);
	static Database db = new Database();
	static Customer cust = new Customer();
	static DeliveryDocket delDoc = new DeliveryDocket();
	static Login login = new Login();
	static DeliveryArea delArea = new DeliveryArea();
	static Driver driver = new Driver();
	static Publication publication = new Publication();
	static Subscription subscription = new Subscription();

	public static void main(String args[]) {
		System.out.println("Welcome to your NewsAgent");
		loginScreen();
	}

	public static void loginScreen() {
		db.initDBConnection();
		System.out.println("Please enter your username: ");
		String username = Console.readLine();
		System.out.println("Please enter your password: ");
		String password = Console.readLine();
		if (login.login(username, password, db)) {
			selectScreen();
		} else if (login.driverLogin(username, password, db)) {
			driverLoginScreen();
		} else {
			System.out.println("Invalid Login");
			loginScreen();
		}
	}

	private static void selectScreen() {

		System.out.println("Please choose and option: ");
		System.out.println("1: View Customer");
		System.out.println("2: View Driver");
		System.out.println("3: View Delivery Area");
		System.out.println("4: View Publication");
		System.out.println("5: View Subscription");
		System.out.println("6: View Delivery Docket");
		System.out.println("7: Logout");
		int choice = Console.readInt("");

		if (choice == 1) {
			customerScreen();
		} else if (choice == 2) {
			driverScreen();
		} else if (choice == 3) {
			deliveryAreaScreen();
		} else if (choice == 4) {
			publicationScreen();
		} else if (choice == 5) {
			subscriptionScreen();
		} else if (choice == 6) {
			delDocketScreen();
		} else if (choice == 7) {
			db.close();
			loginScreen();
		}
	}

	private static void delDocketScreen() {
		System.out.println("Please choose and option: ");
		System.out.println("1: Create Delivery Docket");
		System.out.println("2: Lookup Delivery Docket");
		System.out.println("3: Modify Delivery Docket");
		System.out.println("4: Add Sub to Delivery Docket");
		System.out.println("5: Archive Delivery Docket");
		System.out.println("6: Set delivery status");
		System.out.println("7: Reasign Delivery Docket");
		System.out.println("8: Exit Delivery Docket");
		int choice = scanner.nextInt();
		if (choice == 1) {
			int driverID = Console.readInt("Please enter Delivery Docket Driver ID: ");
			if (delDoc.createDelDock(driverID, db)) {
				System.out.println("Delivery Docket created successfully");
				String choice2 = Console.readLine("Would you like to add a Subscrption to this docket? (Y/N)");
				if (choice2.toLowerCase().equals("y")) {
					addSubToDelDockScreen();
				} else {
					delDocketScreen();
				}
			} else {
				System.out.println("Delivery Docket not created successfully");
				delDocketScreen();
			}
		} else if (choice == 2) {
			delDocLookupScreen();
		} else if (choice == 3) {
			int docketid = Console.readInt("Please enter docket id: ");
			int driverid = Console.readInt("Please enter driver id: ");
			

			if (delDoc.modifyDeliveryDocket(driverid, docketid, db)) {
				System.out.println("Driver successfully modified");
				delDocketScreen();
			} else {
				System.out.println("Driver not successfully modified");
				delDocketScreen();}
		} else if (choice == 4) {
			addSubToDelDockScreen();
		} else if (choice == 5) {
			deleteDelDockScreen();
		} else if (choice == 6) {
			deliveryStatusScreen();
		} else if (choice == 7) {
			
			int docketid = Console.readInt("Please enter docket ID: ");
			int driverid = Console.readInt("Please enter driver ID: ");
			

			if (delDoc.modifyDeliveryDocket(driverid, docketid, db)) {
				System.out.println("Docket info successfully modified");
				delDocketScreen();
			} else {
				System.out.println("Docket info not successfully modified");
				delDocketScreen();}
		
		} else if (choice == 8) {
			selectScreen();
		}
	}

	private static void deliveryStatusScreen() {
		int delDockID = Console.readInt("Please enter Delivery Docket ID: ");
		String status = Console.readLine("Was the delivery successful? (Y/N)");
		if (status.toLowerCase().equals("y")) {
			System.out.println(delDoc.completeDeliveryDocket(delDockID, true, db));
		} else {
			System.out.println(delDoc.completeDeliveryDocket(delDockID, false, db));
		}
		delDocketScreen();
	}

	private static void addSubToDelDockScreen() {
		int delDockID = Console.readInt("Please enter Delivery Docket ID: ");
		int subID = Console.readInt("Please enter Subscription ID you wish to add: ");
		System.out.println(db.addSubToDelDock(delDockID, subID));
		delDocketScreen();
	}

	private static void deleteDelDockScreen() {
		int Docket_ID = Console.readInt("Please enter the ID of the Docket to delete or 0 to exit: ");
		if(Docket_ID==0){
			delDocketScreen();
		} else {
			System.out.println(delDoc.archiveDelDock(Docket_ID, db));

			deleteDelDockScreen();
		}
	}


	private static void delDocLookupScreen() {
		int delDocID = Console.readInt("Please enter Delivery Docket ID or 0 to exit");
		if (delDocID == 0) {
			delDocketScreen();
		}
		String lookup = delDoc.lookUpDelDock(delDocID, db);
		if (lookup.equals("Customer Not Found")) {
			delDocLookupScreen();
		} else
			delDocLookupScreen();
	}

	private static void subscriptionScreen() {
		System.out.println("Please choose and option: ");
		System.out.println("1: Create Subscription");
		System.out.println("2: Lookup Subscription");
		System.out.println("3: Add Publication to Subscription");
		System.out.println("4: Modify Subscription");
		System.out.println("5: Archive Subscription");
		System.out.println("6: Exit Subscription");
		int choice = scanner.nextInt();
		if (choice == 1) {
			String freq = Console.readLine("Please enter Subscription frequency: ");
			if (subscription.createSubscritpion(freq, db)) {
				System.out.println("Subscription created successfully");
				subscriptionScreen();
			} else {
				System.out.println("Subscription not created successfully");
				subscriptionScreen();
			}
		} else if (choice == 2) {
			int ID = Console.readInt("Please enter Subscription ID or press 0 to exit: ");
			if (ID == 0) {
				selectScreen();
			} else {
				String res = subscription.subLookUp(ID, db);
				System.out.println(res);


				subscriptionScreen();}
				subscriptionScreen();
			}
		else if (choice == 3) {
			addPubToSubScreen();

		} else if (choice == 4) {
			int subid = Console.readInt("Please enter Sub id to modify: ");
			String subFreq = Console.readLine("Please enter new subscritpion frequency: ");
			String active = Console.readLine("Please enter new subscritpion status (T/F): ");
			boolean b = false;
			if(active.toLowerCase().equals("t")){
				b = true;
			} else if (active.toLowerCase().equals("f")) {
				b = false;
			}
			if (subscription.modifySubscription(subid, subFreq, b, db) ) {
				b = true;
				System.out.println("Sub successfully modified");
				subscriptionScreen();
				
			} else {
				System.out.println("Sub not successfully modified");
				subscriptionScreen();
			}
		} else if (choice == 5) {
			int Sub_ID = Console.readInt("Please enter the ID of the Sub to archive or 0 to exit: ");
			if(Sub_ID==0){
				System.out.println("Subscription not archived successfully");
				subscriptionScreen();
			} else {
				System.out.println(subscription.archiveSub(Sub_ID, db));
				System.out.println("Subscription archived successfully");
				subscriptionScreen();;
				subscriptionScreen();
				;
			}
		} else if (choice == 6) {
			selectScreen();
		}
}

	

	private static void addPubToSubScreen() {
		int subId = Console.readInt("Please enter Subscription ID: ");
		int pubId = Console.readInt("Please enter Publication ID you wish to add: ");
		System.out.println(db.addPubToSub(subId, pubId));
		subscriptionScreen();
	}

	private static void customerScreen() {
		System.out.println("Please choose and option: ");
		System.out.println("1: Create Customer");
		System.out.println("2: Lookup Customer");
		System.out.println("3: Modify Customer");
		System.out.println("4: Archive Customer");
		System.out.println("5: Exit Customer");
		int choice = scanner.nextInt();
		if (choice == 1) {
			String name = Console.readLine("Please enter customer name: ");
			String address = Console.readLine("Please enter customer address: ");
			int age = Console.readInt("Please enter customer age: ");
			int delArea_id = Console.readInt("Please enter customer delivery area id: ");
			if (cust.createCust(name, address, age, delArea_id, db)) {
				System.out.println("Customer created successfully with ID: " + db.getCustID(name));
				customerScreen();
			} else {
				System.out.println("Customer not created successfully");
				customerScreen();
			}

		} else if (choice == 2) {
			custLookupScreen();
		} else if (choice == 3) {
		} else if (choice == 4) {
			custArchiveScreen();
		} else if (choice == 5) {
			selectScreen();
		}
	}

	private static void custArchiveScreen() {
		int cust_ID = Console.readInt("Please enter the ID of the customer to delete or 0 to exit: ");
		if (cust_ID == 0) {
			customerScreen();
		} else {
			System.out.println(cust.archiveCust(cust_ID, db));
			custArchiveScreen();
		}
	}

	private static void custLookupScreen() {
		int choice = Console.readInt("Please choose search method or 0 to exit:" + "\n1: By name" + "\n2: By ID");
		if (choice == 0) {
			customerScreen();
		}
		if (choice == 1) {
			String name = Console.readLine("Please enter customer name");
			cust.lookupCustByName(name, db);
		} else if (choice == 2) {
			int custID = Console.readInt("Please enter customer ID");
			cust.lookupCustByID(custID, db);
		}
		custLookupScreen();
	}

	private static void driverScreen() {
		System.out.println("Please choose and option: ");
		System.out.println("1: Create Driver");
		System.out.println("2: Lookup Driver");
		System.out.println("3: Modify Driver");
		System.out.println("4: Archive Driver");
		System.out.println("5: Exit Driver");
		int choice = scanner.nextInt();
		if (choice == 1) {
			String name = Console.readLine("Please enter driver name: ");
			String password = Console.readLine("Please enter driver password: ");
			String driverID="";

			if (driver.createDriver(name, password, db)) {
				driverID = db.lookupDriverByName(name);
				System.out.println("Driver created successfully with ID: " + driverID);
				driverScreen();
			} else {  
				System.out.println("Driver not created successfully");
				driverScreen();
			}
		} else if (choice == 2) {
			
			
			
				int driverID = Console.readInt("Please enter Driver ID or press 0 to exit: ");
				if (driverID == 0) {
					selectScreen();
				} else {
					String res = driver.lookupDriver(driverID, db);

					System.out.println(res);

					driverScreen();
				}
				
		} else if (choice == 3) {
		int driverID = Console.readInt("Please enter Driver id: ");
		String driverName = Console.readLine("Please enter Driver name: ");
		String password = Console.readLine("Please enter Driver password: ");
		
		if (driver.modifyDriver(driverID, driverName, password, db)) {
			System.out.println("Driver successfully modified");
			publicationScreen();
		} else {
			System.out.println("Driver not successfully modified");
			publicationScreen();
		}
		
		} else if (choice == 4) {

			int driverID = Console.readInt("Please enter the ID of the Driver to delete or 0 to exit: ");
			if (driverID == 0) {
				System.out.println("Driver not archived successfully");
				driverScreen();
			} else {
				System.out.println(driver.archiveDriver(driverID, db));
				System.out.println("Driver archived successfully");
				driverScreen();
				;
			}
		}

		else if (choice == 5) {
			selectScreen();
		}
	}

	private static void deliveryAreaScreen() {
		System.out.println("Please choose and option: ");
		System.out.println("1: Create Delivery Area");
		System.out.println("2: Lookup Delivery Area");
		System.out.println("3: Modify Delivery Area");
		System.out.println("4: Archive Delivery Area");
		System.out.println("5: Exit Delivery Area");
		int choice = scanner.nextInt();
		if (choice == 1) {
			int delArea_id = Console.readInt("Please enter delivery area id: ");
			String delAreaLoc = Console.readLine("Please enter delivery area location: ");

			if (delArea.createDeliveryArea(delArea_id, delAreaLoc, db)) {
				System.out.println("Delivery Area created successfully");
				deliveryAreaScreen();
			} else {
				System.out.println("Delivery Area not created successfully");
				deliveryAreaScreen();
			}

		} else if (choice == 2) {
			delAreaLookupScreen();
		} else if (choice == 3) {
			int delid = Console.readInt("Please enter docket id: ");
			String delarea = Console.readLine("Please enter driver id: ");

			if (delArea.modifyDeliveryArea(delid, delarea, db)) {
				System.out.println("Area successfully modified");
				deliveryAreaScreen();
			} else {
				System.out.println("Area not successfully modified");
				deliveryAreaScreen();}			
		} else if (choice == 4) {
			int del_ID = Console.readInt("Please enter the ID of the Delivery Area to delete or 0 to exit: ");
			if (del_ID == 0) {
				System.out.println("Delivery Area not archived successfully");
			if(del_ID==0){
				deliveryAreaScreen();
			} else {
				System.out.println(delArea.archiveDelArea(del_ID, db));
				System.out.println("Delivery Area archived successfully");
				deliveryAreaScreen();
			}
			}
		} else if (choice == 5) {
			selectScreen();
		}
	}


	private static void delAreaLookupScreen() {
		int delAreaID = Console.readInt("Please enter Delivery Area ID or 0 to exit");
		if (delAreaID == 0) {
			deliveryAreaScreen();
		}
		String lookup = delArea.lookupArea(delAreaID, db);
		if (lookup.equals("Delivery Area Not Found")) {
			delAreaLookupScreen();
		} else
			delAreaLookupScreen();
	}

	private static void publicationScreen() {
		System.out.println("Please choose and option: ");
		System.out.println("1: Create Publication");
		System.out.println("2: Lookup Publication"); 
		System.out.println("3: Modify Publication");
		System.out.println("4: Archive Publication");
		System.out.println("5: Exit Publication");
		int choice = scanner.nextInt();
		if (choice == 1) {
			String name = Console.readLine("Please enter publication name: ");
			double cost = Console.readDouble("Please enter publication cost: ");
			if (publication.createPublication(name, cost, db)) {
				System.out.println("Publication created successfully with publication ID: " + db.getPubID(name));
				publicationScreen();
			} else {
				System.out.println("Publication not created successfully");
				publicationScreen();
			}

		} else if (choice == 2) {
			int ID = Console.readInt("Please enter publication ID or press 0 to exit: ");
			if (ID == 0) {
				selectScreen();
			} else {
				String res = publication.pubLookUp(ID, db);
				//System.out.println(res);
				publicationScreen();
			}
		} else if (choice == 3) {
			int pubID = Console.readInt("Please enter publication id: ");
			String name = Console.readLine("Please enter publication name: ");
			double cost = Console.readDouble("Please enter publication cost: ");

			if (publication.modifyPublication(pubID, name, cost, db)) {
				System.out.println("Publication successfully modified");
				publicationScreen();
			} else {
				System.out.println("Publication not successfully modified");
				publicationScreen(); 
			}
		} else if (choice == 4) {
			int Pub_ID = Console.readInt("Please enter the ID of the Publication to delete or 0 to exit: ");

			if (Pub_ID == 0) {
				System.out.println("Publication not archived successfully");
				publicationScreen();
			} else {
				System.out.println(publication.archivePub(Pub_ID, db));
				System.out.println("Publication archived successfully");
				publicationScreen();
			}
		} else if (choice == 5) {
			selectScreen();
		}
	}
	
	private static void driverLoginScreen() {
		System.out.println("Please choose and option: ");
		System.out.println("1: View Delivery Area");
		System.out.println("2: View Delivery Docket"); 	
		System.out.println("3: Exit"); 	
		int choice = scanner.nextInt();
		if (choice == 1) {
			delAreaLookupScreen();
		} else if (choice == 2) {
			delDocLookupScreen();
		}else if(choice == 3) {
			db.close();
			loginScreen();
		}
	}
}