import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

public class DeliveryDocketTest extends TestCase {
	Database db = new Database();
	
	/*
	 * Test Number 1
	 * Test Objective: Test valid createDelDock
	 * Input: driverID = 1, subID = 1
	 * Expected Output = true
	 */
	public void testValidCreateDelDock001(){
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals(true, tObject.createDelDock(1,db));
	}
	
	/*
	 * Test Number 2
	 * Test Objective: Test invalid driverID
	 * Input: Name = driverID = -1, subID = 1
	 * Expected Output = false
	 */
	
	public void testInvalidCreateDelDock002(){
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals(false, tObject.createDelDock(-1,db));
	}
	
	/*
	 * Test Number 4
	 * Test Objective: Test valid delivery docket lookup
	 * Input: Delivery Docket ID = 1
	 * Expected Output = Driver ID: 1
	 */
	
	public void testValidLookUpDelDock004(){
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals("Driver ID: 1, Sub ID's: null, Delivered: 0", tObject.lookUpDelDock(1, db));
	}
	
	/*
	 * Test Number 5
	 * Test Objective: Test invalid delivery docket lookup not found
	 * Input: Delivery Docket ID = 999
	 * Expected Output = Delivery Docket Not Found
	 */
	
	public void testValidLookUpDelDock005(){
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals("Delivery Docket Not Found", tObject.lookUpDelDock(999 ,db));
	}
	
	/*
	 * Test Number 6
	 * Test Objective: Test invalid delivery docket lookup
	 * Input: Delivery Docket ID = -1
	 * Expected Output = Invalid Delivery Area ID
	 */
	
	public void testInvalidLookUpDelDock006(){
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals("Invalid Delivery Area ID", tObject.lookUpDelDock(-1 ,db));
	}

	/*
	 * Test Number 7
	 * Test Objective: Test valid add sub to del dock
	 * Input: Delivery Docket ID = 1, Sub ID: 1
	 * Expected Output = "Subscription ID 1 added to Delivery Docket ID 1"
	 */
	
	public void testValidAddSubToDelDock007(){
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals("Subscription ID 1 added to Delivery Docket ID 1", tObject.addSubToDelDock(1, 1, db));
	}
	
	/*
	 * Test Number 8
	 * Test Objective: Test invalid add sub to del dock
	 * Input: Delivery Docket ID = -1, Sub ID: 1
	 * Expected Output = "Invalid Delivery Area ID"
	 */
	
	public void testValidAddSubToDelDock008(){
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals("Invalid Delivery Docket ID", tObject.addSubToDelDock(-1, 1, db));
	}
	
	/*
	 * Test Number 9
	 * Test Objective: Test invalid add sub to del dock
	 * Input: Delivery Docket ID = 1, Sub ID: -1
	 * Expected Output = "Invalid Subscription ID"
	 */
	
	public void testValidAddSubToDelDock009(){
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals("Invalid Subscription ID", tObject.addSubToDelDock(1, -1, db));
	}
	/*
	 * Test Number 10
	 * Test Objective: Test valid archive Docket
	 * Input: CustID =1
	 * Expected Output = "Successfully Archived DeliveryDocket: 1"
	 */public void testValidArchiveDelDock010(){
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals("Successfully Archived DeliveryDocket: 1", tObject.archiveDelDock(1, db));
	}
	
	/*
	 * Test Number 11
	 * Test Objective: Test invalid archive DeliveryDocket
	 * Input: CustID = -1
	 * Expected Output = "Invalid Docket ID"
	 */
	
	public void testValidArchiveDelDock011(){
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals("Invalid Docket ID", tObject.archiveDelDock(-1, db));
	}
	
	/*
	 * Test Number 17
	 * Test Objective: Test nto found archive DeliveryDocket
	 * Input: CustID = 999
	 * Expected Output = "Delivery Docket Not Found"
	 */
	
	public void testValidArchiveDelDockNotFound012(){
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals("Delivery Docket Not Found", tObject.archiveDelDock(999, db));
	}


	
	/*
	 * Test No.13 TestObjective: Test valid modify DelDocket
	 * . Input(s): Delid(1)
	 *  Expected Output(s): "valid driver id"
	 */
	
	public void testValidmodifyDelDocket013() {
		DeliveryDocket tObject = new DeliveryDocket();
		db.initDBConnection();
		assertEquals(true, tObject.modifyDeliveryDocket(1,1,db));
	}
	/*
	 * Test No.14 TestObjective: Test valid modify DelDocket
	 * . Input(s): Delid(1)
	 *  Expected Output(s): "invalid  Driver id"
	 */
	public void testInvalidmodifyPubID014() {
		DeliveryDocket tObject = new DeliveryDocket();
		assertEquals(false,tObject.modifyDeliveryDocket(-1,1,db));
	}	
	/*
	 * Test No.15 TestObjective: Test valid modify DelDocket
	 * . Input(s): Delid(999)
	 *  Expected Output(s): "Driver not found"
	 */
	public void testInvalidmodifyPubID015() {
		DeliveryDocket tObject = new DeliveryDocket();
		assertEquals(false,tObject.modifyDeliveryDocket(1,999,db));
	}	
	/*
	 * Test No.16 TestObjective: Test valid modify DelDocket
	 * . Input(s): Delid(999)
	 *  Expected Output(s): "Docket not found"
	 */
	public void testInvalidmodifyPubID016() {
		DeliveryDocket tObject = new DeliveryDocket();
		assertEquals(false,tObject.modifyDeliveryDocket(1,-1,db));
	}	
	
	
	
	
	
	
	
	
}
