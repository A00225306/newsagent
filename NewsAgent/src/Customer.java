class Customer {

	public boolean createCust(String name, String address, int age, int delAreaID, Database db) {
		boolean res = false;
		try {
			if (name.matches(".*\\d+.*") || name.length() > 40) {
				throw new ExceptionHandler("Invalid Customer Name");
			} else if (age < 18 || age > 120) {
				throw new ExceptionHandler("Invalid Customer Age");
			} else if (address.length() < 30 || address.length() > 80) {
				throw new ExceptionHandler("Invalid Customer Address");
			} else if (delAreaID < 0) {
				throw new ExceptionHandler("Invalid Delivery Area");
			} else {
				db.createCust(name, address, age, delAreaID);
				res = true;
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
		}
		return res;
	}

	public String lookupCustByID(int Cust_ID, Database db) {
		String res = "";
		try {
			if (Cust_ID < 1) {
				throw new ExceptionHandler("Invalid Customer ID");
			} else {
				res = db.lookupCustByID(Cust_ID);
				System.out.println(res);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res;
	}

	public String lookupCustByName(String Cust_Name, Database db) {
		String res = "";
		try {
			if (Cust_Name.matches(".*\\d+.*") || Cust_Name.length() > 40) {
				throw new ExceptionHandler("Invalid Customer Name");
			} else {
				res = db.lookupCustByName(Cust_Name);
				System.out.println(res);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res;
	}

	public String archiveCust(int Cust_ID, Database db) {
		String res = "";
		try {
			if (Cust_ID < 1) {
				throw new ExceptionHandler("Invalid Customer ID");
			} else if (db.lookupCustByID(Cust_ID).equals("Customer Not Found")) {
				res = "Customer Not Found";
			} else {
				res = db.archiveCust(Cust_ID);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res;
	}

	public boolean modifyCust(int custId, String name, String address, int age, int delAreaID, Database db) {
		boolean res = false;
		try {
			if (custId < 1 || custId > 9999) {
				throw new ExceptionHandler("Invalid Customer Id");
			} else if (age < 18 || age > 120) {
				throw new ExceptionHandler("Invalid Customer Age");
			} else if (address.length() < 30 || address.length() > 80) {
				throw new ExceptionHandler("Invalid Customer Address");
			} else if (delAreaID < 0) {
				throw new ExceptionHandler("Invalid Delivery Area");
			} else if (db.modifyCust(custId, name, address, age, delAreaID)) {
				res = true;
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
		}
		return res;
	}

	public String createInvoice(int custID, Database db) {
		String res = "";
		try {
			if (custID < 1 || custID > 9999) {
				throw new ExceptionHandler("Invalid Customer Id");
			} else {
				res = db.createInvoice(custID);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res;
	}
}
