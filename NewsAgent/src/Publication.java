
public class Publication {

	public boolean createPublication(String pubName, double pubCost, Database db) {
		boolean res = false;
		try {
			if (pubName.length() < 2 || pubName.length() > 20) {
				throw new ExceptionHandler("Invalid Publication Name");
			} else if (pubCost < 0.1 || pubCost > 10000) {
				throw new ExceptionHandler("Invalid Publication Cost");
			} else{
				db.createPub(pubName, pubCost); 
				res = true;
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
		}
		return res;
	}
 
	public String pubLookUp(int pubID, Database db) {
		String res = "";
		try {
			if (pubID < 1) {
				throw new ExceptionHandler("Invalid Publication ID");
			} else {
				res = db.lookUpPubID(pubID);
				System.out.println(res);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}

		return res;
	}

	public boolean modifyPublication(int pubID, String pubName, double pubCost, Database db) {
		boolean res = false;
		try {
			if (pubID < 1 || pubID > 40) {
				throw new ExceptionHandler("Invalid ID");
			}
			if (pubName.length() < 2 || pubName.length() > 20) {
				throw new ExceptionHandler("Invalid Publication Name");
			} else if (pubCost < 0.1 || pubCost > 10000) {
				throw new ExceptionHandler("Invalid Publication Cost");
			} else if (db.modifyPub(pubID, pubName, pubCost)) {
				res = true;
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
		}
		return res;
	}

	public String archivePub(int PubID, Database db) {
		String res = "";
		try {
			if (PubID < 1) {
				throw new ExceptionHandler("Invalid Publication ID");
			} else if (db.lookUpPubID(PubID).equals("Publication Not Found")) {
				res = "Publication Not Found";
			} else {
				res = db.archivePub(PubID);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res; 
	}

}
