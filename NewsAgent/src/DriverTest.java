import junit.framework.TestCase;

public class DriverTest extends TestCase {
	Database db = new Database();
	/*
	 * Test Number 1 Test Objective: Test valid create Driver Input: Name = *
	 * Ayokunle Akindele,login = password Expected Output = true
	 */

	public void testCreateDriver001() {
		Driver tObject = new Driver();
		db.initDBConnection();
		assertEquals(true, tObject.createDriver("TEST Driver", "TESTpassword", db));
	}

	/*
	 * Test Number 2 Test Objective: Test invalid create Driver Name Input: Name
	 * = Ay0@*1kunle Akindele, login = password Expected Output = false
	 */

	public void testCreateDriver002() {
		Driver tObject = new Driver();
		assertEquals(false, tObject.createDriver("Ay0@*1kunle Akindele", "password", db));

	}

	/*
	 * Test Number 3 Test Objective: Test invalid create Driver Name too long
	 * Input: Name = Ay0@*1kunle Akindele, login = password Expected Output =
	 * false
	 */

	public void testCreateDriver003() {
		Driver tObject = new Driver();
		assertEquals(false, tObject.createDriver("Ayokunle Akindele666666666666666666666666666666666", "password", db));
	}

	/*
	 * Test Number 4 Test Objective: Test invalid create password too short
	 * Input: Name = Ay0@*1kunle Akindele, login = password Expected Output =
	 * false
	 */

	public void testCreateDriver004() {
		Driver tObject = new Driver();
		assertEquals(false, tObject.createDriver("Ayokunle Akindele", "Pass", db));
	}

	/*
	 * Test Number 5 Test Objective: Test invalid create password too long
	 * Input: Name = Ay0@*1kunle Akindele, login = password Expected Output =
	 * false
	 */

	public void testCreateDriver005() {
		Driver tObject = new Driver();
		assertEquals(false, tObject.createDriver("Ayokunle Akindele", "Password.............................", db));
	}

	/*
	 * Test Number 6 Test Objective: Test valid user and password Test valid
	 * driver login Input: DriverId = 1 Expected Output = true
	 */

	public void testCreateDriver006() {
		Driver tObject = new Driver();
		db.initDBConnection();
		String testDriverLookup = "Driver Name: TEST Driver, Driver Password: TESTpassword";
		assertEquals(testDriverLookup, tObject.lookupDriver(1, db));
	}

	/*
	 * Test Number 7 Test Objective: Test invalid user and password Input:
	 * DriverId = -1 Expected Output = "Invalid Driver ID"
	 */

	public void testinvalidDriverLookup007() {
		Driver tObject = new Driver();
		db.initDBConnection();
		assertEquals("Invalid Driver ID", tObject.lookupDriver(-1, db));
	}

	/*
	 * Test Number 8 Test Objective: Test invalid user and password Test invalid
	 * driver login Input: DriverId = 999 Expected Output = "Driver Not Found"
	 */

	public void testInvalidDriverLookup008() {
		Driver tObject = new Driver();
		db.initDBConnection();
		assertEquals("Driver Not Found", tObject.lookupDriver(999, db));
	}

	/*
	 * Test: Number 9 Test: Objective: Modify from create driver table Input:
	 * DriverId = 1, Name= Ayo Akin, password = password Expected Output = true
	 */

	public void testModifyDrive009() {

		Driver tObject = new Driver();
		db.initDBConnection();
		assertEquals(true, tObject.modifyDriver(1, "TEST Driver", "TESTpassword", db));
	}

	/*
	 * Test: Number 10 Test: Objective: test invalid user id Input: DriverId =
	 * -1, Name= Ayo Akin, password = password Expected Output =
	 * "invalid Not Found"
	 */

	public void testInvalidModifyDrive0010() {

		Driver tObject = new Driver();
		db.initDBConnection();
		assertEquals(false, tObject.modifyDriver(-1, "TEST Driver", "TESTpassword", db));
	}

	/*
	 * Test: Number 11 Test: Objective: test invalid user id Input: DriverId =
	 * 999, Name= Ayo Akin, password = password Expected Output =
	 * "Driver Not Found"
	 */

	public void testInvalidModifyDrive0011() {

		Driver tObject = new Driver();
		db.initDBConnection();
		assertEquals(false, tObject.modifyDriver(999, "TEST Driver", "TESTpassword", db));
	}

	/*
	 * Test Number 12 Test Objective: Test valid archive Driver Input: Driverid
	 * = 1 Expected Output = "Successfully Archived DriverID: 1"
	 */

	public void testValidArchiveDriver012() {
		Driver tObject = new Driver();
		db.initDBConnection();
		assertEquals("Successfully Archived Driver ID: 1", tObject.archiveDriver(1, db));
	}

	/*
	 * Test Number 13 Test Objective: Test invalid archive Driver Input:
	 * Driverid = -1 Expected Output = "Invalid Customer ID"
	 */

	public void testValidArchiveDriver013() {
		Driver tObject = new Driver();
		db.initDBConnection();
		assertEquals("Invalid Driver ID", tObject.archiveDriver(-1, db));
	}

	/*
	 * Test Number 14 Test Objective: Test valid archive Driver Input: CustID =
	 * 999 Expected Output = "Customer Not Found"
	 */

	public void testValidArchiveDriver014() {
		Driver tObject = new Driver();
		db.initDBConnection();
		assertEquals("Driver Not Found", tObject.archiveDriver(999, db));
	}

}