import junit.framework.TestCase;

public class PublicationTest extends TestCase {
	Database db = new Database();

	/*
	 * Test No.1 TestObjective: Test invalid Create pubName. Input(s):
	 * pubName(l), pubCost(9.00). Expected Output(s): false, print to console "Invalid Publication Name"
	 */
	public void testInvalidCreatePubName001() {
		Publication tObject = new Publication();
		assertEquals(false,tObject.createPublication("l", 9.00,db));
	}

	/*
	 * Test No.2 TestObjective: Test valid Create pubName. Input(s): 
	 * pubName(#5), pubCost(9.00). Expected Output(s): true
	 */
	public void testValidCreatePubName002() {
		Publication tObject = new Publication();
		db.initDBConnection();
		assertEquals(true, tObject.createPublication("TestPub", 9.99,db));
	}

	/*
	 * Test No.3 TestObjective: Test invalid Create pubName. Input(s): 
	 * pubName(abcdefghijklmnopqrstuvwxyz), pubCost(9.00). Expected Output(s): "Invalid Publication Name"
	 */
	public void testInvalidCreatePubName003() {
		Publication tObject = new Publication();
		assertEquals(false,tObject.createPublication("abcdefghijklmnopqrstuvwxyz", 9.00,db));
	}

	/*
	 * Test No.4 TestObjective: Test valid Create pubCost. Input(s): 
	 * pubName(Inside United), pubCost(10.00). Expected Output(s): true
	 */
	public void testValidCreatePubCost004() {
		db.initDBConnection();
		Publication tObject = new Publication();
		assertEquals(true, tObject.createPublication("TEST Pub", 10.00,db));
	}

	/*
	 * Test No.5 TestObjective: Test invalid Create pubCost. Input(s):
	 * pubName(Inside United), pubCost(-10.00). Expected Output(s): "Invalid Publication Cost"
	 */
	public void testInvalidCreatePubCost005() {
		Publication tObject = new Publication();
		assertEquals(false,tObject.createPublication("Inside United", -10.00,db));
	}
	/*
	 * Test No.6 TestObjective: Test invalid Create pubCost. Input(s):
	 * pubName(Inside United), pubCost(-10.00). Expected Output(s): "Invalid Publication Cost"
	 */
	public void testInvalidCreatePubCost006() {
		Publication tObject = new Publication();
		assertEquals(false,tObject.createPublication("Inside United", 20000,db));
	}	
	
	/*
	 * Test No.7 TestObjective: Test invalid publication lookup. Input(s):
	 * pudID(-5). Expected Output(s): false, print to console "Invalid Publication ID"
	 */
	public void testInvalidpubLookUp007() {
		Publication tObject = new Publication();
		db.initDBConnection();
		assertEquals("Invalid Publication ID", tObject.pubLookUp(-5, db));
	}
	
	/*
	 * Test No.8 TestObjective: Test valid publication lookup. Input(s):
	 * pudID(1). Expected Output(s): true
	 */
	public void testValidpubLookUp008() {
		Publication tObject = new Publication();
		db.initDBConnection();
		String testpub = "Name of publication: TestPub, Cost of publication: 9.99";
		assertEquals(testpub, tObject.pubLookUp(1, db));
	}
	
	
	
	/*
	 * Test No.1 TestObjective: Test invalid Create pubName. Input(s):
	 * pubName(l), pubCost(9.00). Expected Output(s): false, print to console "Invalid Publication Name"
	 */
	public void testInvalidmodifyPubName001() {
		Publication tObject = new Publication();
		assertEquals(false,tObject.modifyPublication(1,"a", 99.00,db));
	}

	/*
	 * Test No.2 TestObjective: Test valid Create pubName. Input(s): 
	 * pubName(#5), pubCost(9.00). Expected Output(s): true
	 */
	public void testValidmodifyPubName002() {
		Publication tObject = new Publication();
		db.initDBConnection();
		assertEquals(true, tObject.modifyPublication(1,"TestPubMod", 99.99,db));
	}

	/*
	 * Test No.3 TestObjective: Test invalid Create pubName. Input(s): 
	 * pubName(abcdefghijklmnopqrstuvwxyz), pubCost(9.00). Expected Output(s): "Invalid Publication Name"
	 */
	public void testInvalidmodifyPubName003() {
		Publication tObject = new Publication();
		assertEquals(false,tObject.modifyPublication(1,"abcdefghijklmnopqrstuvwxyz", 9.00,db));
	}

	/*
	 * Test No.5 TestObjective: Test invalid Create pubCost. Input(s):
	 * pubName(Inside United), pubCost(-10.00). Expected Output(s): "Invalid Publication Cost"
	 */
	public void testInvalidmodifyPubCost005() {
		Publication tObject = new Publication();
		assertEquals(false,tObject.modifyPublication(1,"Inside United", -10.00,db));
	}
	/*
	 * Test No.6 TestObjective: Test invalid modify pubCost
	 * . Input(s): pubName(Inside United), pubCost(-10.00).
	 *  Expected Output(s): "Invalid Publication Cost"
	 */
	public void testInvalidmodifyPubCost006() {
		Publication tObject = new Publication();
		assertEquals(false,tObject.modifyPublication(1,"Inside United", 20000,db));
	}	
	/*
	 * Test No.7 TestObjective: Test valid modify pubCost
	 * . Input(s): pubName(Inside United), pubCost(-10.00).
	 *  Expected Output(s): "Invalid Publication Cost"
	 */
	
	public void testValidmodifyPubID007() {
		Publication tObject = new Publication();
		db.initDBConnection();
		assertEquals(true, tObject.modifyPublication(11,"TestPubMod", 99.99,db));
	}
	/*
	 * Test No.8 TestObjective: Test invalid modify pubCost
	 * . Input(s): pubName(Inside United), pubCost(200000).
	 *  Expected Output(s): "Invalid ID"
	 */
	public void testInvalidmodifyPubID008() {
		Publication tObject = new Publication();
		assertEquals(false,tObject.modifyPublication(-1,"Inside United", 20000,db));
	}	
	/*
	 * Test No.9 TestObjective: Test invalid modify pubCost
	 * . Input(s): id(40) pubName(Inside United), pubCost(20000).
	 *  Expected Output(s): "Invalid ID"
	 */
	public void testInvalidmodifyPubID009() {
		Publication tObject = new Publication();
		assertEquals(false,tObject.modifyPublication(40,"Inside United", 20000,db));
	}	

	/*
	 * Test No. 10
	 * Test Objective: Test valid archive publication
	 * Input: PublicationID = 1
	 * Expected Output = "Successfully Archived PublicationID: 1"
	 */
	
	public void testValidArchivePublication010(){
		Publication tObject = new Publication();
		db.initDBConnection();
		assertEquals("Successfully Archived PublicationID: 1", tObject.archivePub(1, db));
	}
	
	/*
	 * Test No. 11
	 * Test Objective: Test invalid archive publication
	 * Input: PublicationID = -1
	 * Expected Output = "Invalid publication ID"
	 */
	
	public void testInValidArchivePublication011(){
		Publication tObject = new Publication();
		db.initDBConnection();
		assertEquals("Invalid Publication ID", tObject.archivePub(-1, db));
	}
	
	/*
	 * Test No.12
	 * Test Objective: Test archive Publication not found
	 * Input: PublicationID = 999
	 * Expected Output = "Publication Not Found"
	 */
	
	public void testValidArchivePublicationNotFound012(){
		Publication tObject = new Publication();
		db.initDBConnection();
		assertEquals("Publication Not Found", tObject.archivePub(999, db));
	}
	
}
