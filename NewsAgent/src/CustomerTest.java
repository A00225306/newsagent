import junit.framework.TestCase;

public class CustomerTest extends TestCase {
	static Database db = new Database();
	
	/*
	 * Test Number 1
	 * Test Objective: Test valid create customer
	 * Input: Name = TestName, Address = TestAddress, TestAddress, TestAddress, Age = 29, Delivery ID = 17
	 * Expected Output = true
	 */
	
	public void testValidCreateCustomer001(){
		Customer tObject = new Customer();
		db.initDBConnection();
		assertEquals(true, tObject.createCust("TestName", "TestAddress, TestAddress, TestAddress", 29, 1,db));
	}
	
	/*
	 * Test Number 2
	 * Test Objective: Test invalid customer name
	 * Input: Name = Owen 123 Glennon, Address = Mullingar, Age = 29, Delivery ID = 17
	 * Expected Output = false, print to console: "Invalid Customer Name"
	 */
	
	public void testInvalidName002(){
		Customer tObject = new Customer();
		assertEquals(false,tObject.createCust("Owen 123 Glennon", "19 Newtown Lawns, Mullingar, Co.Westmeath", 29, 17,db));
	}
	
	/*
	 * Test Number 3
	 * Test Objective: Test invalid customer age above 120
	 * Input: Name = Owen Glennon, Address = Mullingar, Age = 125, Delivery ID = 17
	 * Expected Output = false, print to console: "Invalid Customer Age"
	 */
	
	public void testInvalidAge003(){
		Customer tObject = new Customer();
		assertEquals(false,tObject.createCust("Owen Glennon", "19 Newtown Lawns, Mullingar, Co.Westmeath", 125, 17,db));
	}
	
	/*
	 * Test Number 4
	 * Test Objective: Test invalid customer age under 18
	 * Input: Name = Owen Glennon, Address = Mullingar, Age = 17, Delivery ID = 17
	 * Expected Output = false, print to console: "Invalid Customer Age"
	 */
	
	public void testInvalidAge004(){
		Customer tObject = new Customer();
		assertEquals(false,tObject.createCust("Owen Glennon", "19 Newtown Lawns, Mullingar, Co.Westmeath", 17, 17,db));
		}
	
	/*
	 * Test Number 5
	 * Test Objective: Test invalid delivery area less than 0
	 * Input: Name = Owen Glennon, Address = Mullingar, Age = 17, Delivery ID = -1
	 * Expected Output = false, print to console "Invalid Delivery Area"
	 */
	
	public void testInvalidDeliveryArea005(){
		Customer tObject = new Customer();
		assertEquals(false,tObject.createCust("Owen Glennon", "19 Newtown Lawns, Mullingar, Co.Westmeath", 25, -1,db));
		}
	
	/*
	 * Test Number 6
	 * Test Objective: Test invalid customer name too long
	 * Input: Name = Owen Glennonnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn, Address = "19 Newtown Lawns, Mullingar, Co.Westmeath", Age = 29, Delivery ID = 17
	 * Expected Output = false, print to console "Invalid Customer Name"
	 */
	
	public void testInvalidName006(){
		Customer tObject = new Customer();
		assertEquals(false,tObject.createCust("Owen Glennonnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn", "19 Newtown Lawns, Mullingar, Co.Westmeath", 29, 17,db));
	}
	
	/*
	 * Test Number 7
	 * Test Objective: Test invalid customer address, less than 30 char
	 * Input: Name = Owen Glennon, Address = Mullingar, Age = 29, Delivery ID = 17
	 * Expected Output = false, print to console "Invalid Customer Address"
	 */
	
	public void testInvalidAddress007(){
		Customer tObject = new Customer();
		assertEquals(false,tObject.createCust("Owen Glennon", "Mullingar", 29, 17,db));
	}
	
	/*
	 * Test Number 8
	 * Test Objective: Test invalid customer address, more than 80 char
	 * Input: Name = Owen Glennon, Address = "Mullingar Mullingar Mullingar Mullingar Mullingar Mullingar Mullingar Mullingar 123", Age = 29, Delivery ID = 17
	 * Expected Output = false
	 */
	
	public void testInvalidAddress008(){
		Customer tObject = new Customer();
		assertEquals(false,tObject.createCust("Owen Glennon", "Mullingar Mullingar Mullingar Mullingar Mullingar Mullingar Mullingar Mullingar 123", 29, 17,db));
	}
	
	/*
	 * Test Number 9
	 * Test Objective: Test valid lookup customer
	 * Input: Cust_ID = 1
	 * Expected Output = Cust Name: TestName, Cust Address: TestAddress, TestAddress, TestAddress, Cust Age: 29, Cust Del Area ID: 1
	 */
	
	public void testValidCustomerLookup009(){
		Customer tObject = new Customer();
		db.initDBConnection();
		String testCust = "Cust Name: TestName, Cust Address: TestAddress, TestAddress, TestAddress, Cust Age: 29, Cust Del Area ID: 1";
		assertEquals(testCust,tObject.lookupCustByID(1,db));
	}
	
	/*
	 * Test Number 10
	 * Test Objective: Test invalid lookup customer
	 * Input: Cust_ID = -4
	 * Expected Output = Invalid Customer ID
	 */
	
	public void testInvalidCustomer010(){
		Customer tObject = new Customer();
		assertEquals("Invalid Customer ID",tObject.lookupCustByID(-4,db));
	}
	
	/*
	 * Test Number 11
	 * Test Objective: Test invalid lookup customer
	 * Input: Cust_ID = 999
	 * Expected Output = Customer Not Found
	 */
	
	public void testValidCustomerLookupNotFound011(){
		Customer tObject = new Customer();
		assertEquals("Customer Not Found",tObject.lookupCustByID(999,db));
	}
	
	/*
	 * Test Number 12
	 * Test Objective: Test valid lookup customer not found
	 * Input: Customer name: TestNameFail
	 * Expected Output = Customer Not Found
	 */
	
	public void testValidCustomerLookupNotFound012(){
		Customer tObject = new Customer();
		assertEquals("Customer Not Found",tObject.lookupCustByName("TestNameFail",db));
	}
	
	/*
	 * Test Number 13
	 * Test Objective: Test valid lookup customer
	 * Input: Cust_ID = TestName
	 * Expected Output = Cust ID: 1, Cust Name: TestName, Cust Address: TestAddress, TestAddress, TestAddress, Cust Age: 29, Cust Del Area ID: 1
	 */
	
	public void testValidCustomerLookupFound013(){
		Customer tObject = new Customer();
		assertEquals("Cust ID: 1, Cust Name: TestName, Cust Address: TestAddress, TestAddress, TestAddress, Cust Age: 29, Cust Del Area ID: 1",tObject.lookupCustByName("TestName",db));
	}
	
	/*
	 * Test Number 14
	 * Test Objective: Test invalid lookup customer
	 * Input: Cust_ID = -1
	 * Expected Output = false
	 */
	
	public void testInvalidCustomerLookup014(){
		Customer tObject = new Customer();
		assertEquals("Invalid Customer Name",tObject.lookupCustByName("Test 123 Name",db));
	}
	
	/*
	 * Test Number 15
	 * Test Objective: Test valid archive customer
	 * Input: CustID = 1
	 * Expected Output = "Successfully Archived CustID: 1"
	 */
	
	public void testValidArchiveCustomer015(){
		Customer tObject = new Customer();
		db.initDBConnection();
		assertEquals("Successfully Archived CustID: 1", tObject.archiveCust(1, db));
	}
	
	/*
	 * Test Number 16
	 * Test Objective: Test invalid archive customer
	 * Input: CustID = -1
	 * Expected Output = "Invalid Customer ID"
	 */
	
	public void testValidArchiveCustomer016(){
		Customer tObject = new Customer();
		db.initDBConnection();
		assertEquals("Invalid Customer ID", tObject.archiveCust(-1, db));
	}
	
	/*
	 * Test Number 17
	 * Test Objective: Test valid archive customer
	 * Input: CustID = 999
	 * Expected Output = "Customer Not Found"
	 */
	
	public void testValidArchiveCustomerNotFound017(){
		Customer tObject = new Customer();
		db.initDBConnection();
		assertEquals("Customer Not Found", tObject.archiveCust(999, db));
	}
	
	
	/*
	 * Test Number 18
	 * Test Objective: Test valid modify customer
	 * Input: Customer id = 1 Name = TestName, Address = TestAddress, TestAddress, TestAddress, Age = 29, Delivery ID = 1
	 * Expected Output = true
	 */
	
	public void testValidModifyCustomer018(){
		Customer tObject = new Customer();
		db.initDBConnection();
		assertEquals(true, tObject.modifyCust(1,"TestName", "TestAddress, TestAddress, TestAddress", 29, 1,db));

	}
	
	
	
	/*
	 * Test Number 19
	 * Test Objective: Test valid modify customer
	 * Input: Customer id = -1 Name = TestName, Address = TestAddress, TestAddress, TestAddress, Age = 29, Delivery ID = 1
	 * Expected Output = invalid customer id
	 */
	
	public void testInvalidModifyCustomer019(){
		Customer tObject = new Customer();
		db.initDBConnection();
		assertEquals(false, tObject.modifyCust(-1,"TestName", "TestAddress, TestAddress, TestAddress", 29, 1,db));

	}

	/*
	 * Test Number 20
	 * Test Objective: Test valid modify customer
	 * Input: Customer id = 999 Name = TestName, Address = TestAddress, TestAddress, TestAddress, Age = 29, Delivery ID = 1
	 * Expected Output = invalid customer id
	 */
	
	public void testInvalidModifyCustomer020(){
		Customer tObject = new Customer();
		db.initDBConnection();
		assertEquals(false, tObject.modifyCust(10000,"TestName", "TestAddress, TestAddress, TestAddress", 29, 1,db));

	}
	
	/*
	 * Test Number 21
	 * Test Objective: Test invalid cust id for create invoice
	 * Input: Customer id = 10000
	 * Expected Output = invalid customer id
	 */
	
	public void testInvalidCreateInvoice021(){
		Customer tObject = new Customer();
		assertEquals("Invalid Customer Id", tObject.createInvoice(10000,db));

	}
	
	/*
	 * Test Number 22
	 * Test Objective: Test valid cust id for create invoice
	 * Input: Customer id = 1
	 * Expected Output = "Invoice Created"
	 */
	
	public void testValidCreateInvoice022(){
		Customer tObject = new Customer();
		db.initDBConnection();
		assertEquals("Invoice Created", tObject.createInvoice(1,db));
	}
	
	
	
}
