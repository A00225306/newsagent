import junit.framework.TestCase;

public class DeliveryAreaTest extends TestCase {
	Database db = new Database();
	/*
	 * Test Number 1
	 * Test Objective: Test valid id valid id area
	 * Input: id = 1, id area Mullingar
	 * Expected Output = true
	 */
	
	
	public void testvalidateArea001() {
		DeliveryArea tObject = new DeliveryArea();
		db.initDBConnection();
		assertEquals(true, tObject.createDeliveryArea(1, "TEST Westmeath",db));
	}
	/*
	 * Test Number 2
	 * Test Objective: Test invalid id  valid id area
	 * Input: id = -1, id area Mullingar
	 * Expected Output = false, print to console "Invalid Delivery ID"
	 */

	public void testvalidateArea002() {
		DeliveryArea tObject = new DeliveryArea();
		assertEquals(false,tObject.createDeliveryArea(-1, "TEST Westmeath",db));
	}

	/*
	 * Test Number 3
	 * Test Objective: Test invalid id  valid id area
	 * Input: id = -1, id area Mullingar
	 * Expected Output = false, print to console "Invalid Delivery ID"
	 */

	public void testvalidateArea003() {
		DeliveryArea tObject = new DeliveryArea();
		assertEquals(false,tObject.createDeliveryArea(101, "TEST Westmeath",db));
	}

	
	
	/*
	 * Test Number 4
	 * Test Objective: Test valid id invalid id area
	 * Input: id = 1, id area abc
	 * Expected Output = false, print to console "Invalid Delivery Area location"
	 */
	
	
	public void testvalidateArea004() {
		DeliveryArea tObject = new DeliveryArea();
		assertEquals(false,tObject.createDeliveryArea(1, "abc",db));
	}

	/*
	 * Test Number 5
	 * Test Objective: Test valid id invalid id area
	 * Input: id = 1, id area 1a@snsdjndabcq1a@snsdjndabcq1a@snsdjndabcq
	 * Expected Output = false, print to console "Invalid Delivery Area location"
	 */
	public void testvalidateArea005() {
		DeliveryArea tObject = new DeliveryArea();
		assertEquals(false,tObject.createDeliveryArea(1, "1a@snsdjndabcq1a@snsdjndabcq1a@snsdjndabcqMHGMHFGDHFJHFGDHFH",db));
	}
	/*
	 * Test Number 6
	 * Test Objective: Test valid lookup delivery area
	 * Input: Del_ID = 1
	 * Expected Output = true
	 */
	
	public void testValidAreaLookup006(){
		DeliveryArea tObject = new DeliveryArea();
		db.initDBConnection();
		String testArea = "areaLocation: TEST Westmeath";
		assertEquals(testArea,tObject.lookupArea(1,db));
	}
	
	/*
	 * Test Number 7
	 * Test Objective: Test invalid lookup delivery area
	 * Input: Cust_ID = -1
	 * Expected Output = false
	 */
	
	public void testInvalidDeliveryArea007(){
		DeliveryArea tObject = new DeliveryArea();
		assertEquals("Invalid Delivery Area ID",tObject.lookupArea(-4,db));
	}
	
	/*
	 * Test Number 8
	 * Test Objective: Test delivery area not found
	 * Input: Cust_ID = 999
	 * Expected Output = false
	 */
	
	public void testValidDeliveryAreaNotFound008(){
		DeliveryArea tObject = new DeliveryArea();
		db.initDBConnection();
		assertEquals("Delivery Area Not Found",tObject.lookupArea(999,db));
	}
	
	
	/*
	 * Test No. 9
	 * Test Objective: Test valid archive delivery Area
	 * Input: deliveryAreaID = 1
	 * Expected Output = "Successfully Archived delAreaID: 1"
	 */
	public void testValidArchiveDelArea001(){
		DeliveryArea tObject = new DeliveryArea();
		db.initDBConnection();
		assertEquals("Successfully Archived DeliveryArea ID: 1", tObject.archiveDelArea(1, db));
	}
	
	/*
	 * Test No. 10
	 * Test Objective: Test invalid archive deliver area
	 * Input: delAreaID = -1
	 * Expected Output = "Invalid delivery area ID"
	 */
	
	public void testInValidArchiveDelArea002(){
		DeliveryArea tObject = new DeliveryArea();
		assertEquals("Invalid DeliveryArea ID", tObject.archiveDelArea(-1, db));
	}
	
	/*
	 * Test No. 10
	 * Test Objective: Test invalid archive deliver area
	 * Input: delAreaID = 999
	 * Expected Output = "Invalid delivery area ID"
	 */
	
	public void testInValidArchiveDelArea003(){
		DeliveryArea tObject = new DeliveryArea();
		assertEquals("Invalid DeliveryArea ID", tObject.archiveDelArea(999, db));
	}
	
	
	
	
	
	
	/*
	 * Test Number 11
	 * Test Objective: Test valid id valid id area
	 * Input: id = 1, id area Mullingar
	 * Expected Output = true
	 */
	
	
	public void testmodifyArea011() {
		DeliveryArea tObject = new DeliveryArea();
		db.initDBConnection();
		assertEquals(true, tObject.createDeliveryArea(1, "TEST Westmeath",db));
	}
	
	/*
	 * Test Number 12
	 * Test Objective: Test invalid id  valid id area
	 * Input: id = -1, id area Mullingar
	 * Expected Output = false, print to console "Invalid Delivery ID"
	 */

	public void testmodifyArea012() {
		DeliveryArea tObject = new DeliveryArea();
		assertEquals(false,tObject.createDeliveryArea(-1, "TEST Westmeath",db));
	}
	

	/*
	 * Test Number 13
	 * Test Objective: Test valid id invalid id area
	 * Input: id = 1, id area abc
	 * Expected Output = false, print to console "Invalid Delivery Area location"
	 */
	
	
	public void testmodifyArea013() {
		DeliveryArea tObject = new DeliveryArea();
		assertEquals(false,tObject.createDeliveryArea(1, "abc",db));
	}
	
}