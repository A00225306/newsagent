import junit.framework.TestCase;

public class LoginTest extends TestCase {
	Database db = new Database();

	
	/*
	 * Test Number 1
	 * Test Objective: Test valid login
	 * Input: Username = admin, Password = password
	 * Expected Output = true
	 */
	
	public void testValidLogin001(){
		Login testObj = new Login();
		assertEquals(true, testObj.login("admin", "password", db));
	}
	
	/*
	 * Test Number 2
	 * Test Objective: Test valid login
	 * Input: Username = asfgdsfg, Password = sdfsdfh
	 * Expected Output = true
	 */
	
	public void testinValidLogin002(){
		Login testObj = new Login();
		assertEquals(false,testObj.login("asfgdsfg", "sdfsdfh",db));
	}

}
