import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Database {

	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	void initDBConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/NewsAgent";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.print("Failed to initialise DB Connection");
		}
	}

	public boolean createCust(String name, String address, int age, int delAreaID) {
		String cmd = ("INSERT INTO Customer(Cust_Name,Cust_Address,Cust_Age,Del_ID) VALUES('" + name + "','" + address
				+ "'," + age + "," + delAreaID + ")");
		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (Exception e1) {
		}
		return false;
	}

	public boolean createDelArea(int delAreaID, String areaLocation) {
		String cmd = ("INSERT INTO DeliveryArea(delAreaID,areaLocation) VALUES(" + delAreaID + ",'" + areaLocation
				+ "')");
		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (Exception e1) {
		}
		return false;
	}

	boolean createDriver(String driverName, String password) {
		String cmd = ("INSERT INTO Driver(driverName,password) VALUES('" + driverName + "','" + password + "')");
		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (Exception e1) {
		}
		return false;
	}

	public boolean createSubscritpion(String frequency) {
		String cmd = ("INSERT INTO Subscription(Cust_ID, pubID, subFreq, active) VALUES(null, null,'" + frequency
				+ "',true)");
		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (Exception e1) {
		}
		return false;
	}

	public boolean createPub(String pubName, double pubCost) {
		String cmd = ("INSERT INTO Publication(pubName, pubCost) VALUES('" + pubName + "'," + pubCost + ");");
		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (Exception e1) {
		}
		return false;
	}

	public boolean modifySub(int subID, String Freq, boolean b) {

		String cmd = ("UPDATE Subscription SET subFreq = '" + Freq + "', active = " + b + " where subID = " + subID
				+ ";");
		System.out.println(cmd);
		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return false;
	}

	public boolean modifyPub(int pubID, String pubName, double pubCost) {
		String cmd = ("UPDATE Publication SET pubName = '" + pubName + "', pubCost = " + pubCost + " where pubID = "
				+ pubID + ";");

		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (Exception e1) {
		}
		return false;
	}

	public boolean modifyDocket(int driverID, int delDockID) {
		String cmd = ("UPDATE DeliveryDocket SET driver_ID = " + driverID + " where DelDock_ID = " + delDockID + ";");
		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (Exception e1) {
			System.out.println();
		}
		return false;
	}

	public String archiveSub(int SubID) {
		String res = "";
		String sqlCmd = "delete from subscription where subID = " + SubID + ";";
		String sqlCmd2 = "select * from sub_after_delete where Cust_ID = " + SubID + ";";
		int Sub_ID = 0;
		try {
			stmt.executeUpdate(sqlCmd);
			rs = stmt.executeQuery(sqlCmd2);
			while (rs.next()) {
				Sub_ID = Integer.parseInt(rs.getString(1));
			}
			if (SubID == Sub_ID) {
				res = "Successfully Archived SubID: " + Sub_ID;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			res = e.getMessage();
		}
		return res;
	}

	public String archivePub(int PubID) {
		String res = "";
		String sqlCmd = "delete from publication where PubID = " + PubID + ";";
		String sqlCmd2 = "select * from publication_archive where PubID = " + PubID + ";";
		int Pub_ID = 0;
		try {
			stmt.executeUpdate(sqlCmd);
			rs = stmt.executeQuery(sqlCmd2);
			while (rs.next()) {
				Pub_ID = Integer.parseInt(rs.getString(1));
			}
			if (PubID == Pub_ID) {
				res = "Successfully Archived PublicationID: " + Pub_ID;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			res = e.getMessage();
		}
		return res;
	}

	public String lookupCustByID(int cust_ID) {
		String Cust_Name = "";
		String Cust_Address = "";
		int Cust_Age = 0;
		int Del_ID = 0;
		String sqlCmd = "Select * from customer where Cust_ID = " + cust_ID + ";";
		try {
			rs = stmt.executeQuery(sqlCmd);
			while (rs.next()) {
				Cust_Name = rs.getString(2);
				Cust_Address = rs.getString(3);
				Cust_Age = Integer.parseInt(rs.getString(4));
				Del_ID = Integer.parseInt(rs.getString(5));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (Cust_Age == 0) {
			return "Customer Not Found";
		} else
			return "Cust Name: " + Cust_Name + ", Cust Address: " + Cust_Address + ", " + "Cust Age: " + Cust_Age
					+ ", Cust Del Area ID: " + Del_ID;
	}

	public String lookupCustByName(String Cust_Name) {
		int cust_id = 0;
		String Cust_Address = "";
		int Cust_Age = 0;
		int Del_ID = 0;
		String sqlCmd = "Select * from customer where Cust_Name = '" + Cust_Name + "';";
		try {
			rs = stmt.executeQuery(sqlCmd);
			while (rs.next()) {
				cust_id = Integer.parseInt(rs.getString(1));
				Cust_Address = rs.getString(3);
				Cust_Age = Integer.parseInt(rs.getString(4));
				Del_ID = Integer.parseInt(rs.getString(5));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (Cust_Age == 0) {
			return "Customer Not Found";
		} else
			return "Cust ID: " + cust_id + ", Cust Name: " + Cust_Name + ", Cust Address: " + Cust_Address + ", "
					+ "Cust Age: " + Cust_Age + ", Cust Del Area ID: " + Del_ID;
	}

	public String lookupDelArea(int Del_ID) {
		String Del_Address = "";
		String sqlCmd2 = "Select * from DeliveryArea where delAreaID = " + Del_ID + ";";
		try {
			rs = stmt.executeQuery(sqlCmd2);
			while (rs.next()) {
				Del_Address = rs.getString(2);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (Del_Address == "") {
			return "Delivery Area Not Found";
		} else
			return "areaLocation: " + Del_Address;
	}

	public boolean createDelDock(int driverID) {
		String cmd = ("INSERT INTO DeliveryDocket(driver_ID, sub_ID, delivered) VALUES(" + driverID
				+ ", null, false);");
		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (Exception e1) {
		}
		return false;
	}

	public String lookupDriver(int DriverId) {
		String DriverName = "";
		String password = "";

		String sqlCmd = "Select * from Driver where DriverId = " + DriverId + ";";
		try {
			rs = stmt.executeQuery(sqlCmd);
			while (rs.next()) {
				DriverName = rs.getString(2);
				password = rs.getString(3);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (DriverName == "") {
			return "Driver Not Found";
		}
		return "Driver Name: " + DriverName + ", Driver Password: " + password;
	}

	public String lookUpDelDock(int delDockID) {
		int Driver_ID = 0;
		String Sub_IDs = "";
		String delivered = "";
		String sqlCmd = "Select * from deliverydocket where DelDock_ID = " + delDockID + ";";
		try {
			rs = stmt.executeQuery(sqlCmd);
			while (rs.next()) {
				Driver_ID = Integer.parseInt(rs.getString(2));
				Sub_IDs = rs.getString(3);
				delivered = rs.getString(4);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (Driver_ID == 0) {
			return "Delivery Docket Not Found";
		} else
			return "Driver ID: " + Driver_ID + ", Sub ID's: " + Sub_IDs + ", " + "Delivered: " + delivered;
	}

	public void close() {
		if (con != null) {
			try {
				con.close();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	public String getCustID(String name) {
		String custID = "";
		String cmd = "Select * from customer where Cust_Name = '" + name + "';";
		try {
			rs = stmt.executeQuery(cmd);
			while (rs.next()) {
				custID = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return custID;
	}

	public String lookUpPubID(int pubID) {
		String nameOfPub = "";
		double costOfPub = 0;

		String sqlCmd = "Select * from Publication where pubID = '" + pubID + "';";
		try {
			rs = stmt.executeQuery(sqlCmd);
			while (rs.next()) {
				nameOfPub = rs.getString(2);
				costOfPub = rs.getDouble(3);
			}
			if (nameOfPub.equals("")) {
				return "Publication Not Found";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "Name of publication: " + nameOfPub + "," + " Cost of publication: " + costOfPub;
	}

	public String getPubID(String name) {
		String PubID = "";
		String cmd = "Select * from publication where pubName = '" + name + "';";
		try {
			rs = stmt.executeQuery(cmd);
			while (rs.next()) {
				PubID = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return PubID;
	}

	public String archiveCust(int cust_ID) {
		String res = "";
		String sqlCmd = "delete from customer where cust_id = " + cust_ID + ";";
		String sqlCmd2 = "select * from customer_archive where cust_id = " + cust_ID + ";";
		int custID = 0;
		try {
			stmt.executeUpdate(sqlCmd);
			rs = stmt.executeQuery(sqlCmd2);
			while (rs.next()) {
				custID = Integer.parseInt(rs.getString(1));
			}
			if (cust_ID == custID) {
				res = "Successfully Archived CustID: " + custID;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			res = e.getMessage();
		}
		return res;
	}

	public String archiveDelArea(int delAreaID) {
		String res = "";
		String sqlCmd = "delete from DeliveryArea where delAreaID = " + delAreaID + ";";
		String sqlCmd2 = "select * from deliveryArea_archive where delAreaID = " + delAreaID + ";";
		int areaID = 0;
		try {
			stmt.executeUpdate(sqlCmd);
			rs = stmt.executeQuery(sqlCmd2);
			while (rs.next()) {
				areaID = Integer.parseInt(rs.getString(1));
			}
			if (delAreaID == areaID) {
				res = "Successfully Archived DeliveryArea ID: " + areaID;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			res = e.getMessage();
		}
		return res;
	}

	public String archiveDriver(int driverId) {
		String res = "";
		String sqlCmd = "delete from driver where DriverId = " + driverId + ";";
		String sqlCmd2 = "select * from driver_archive where driverId = " + driverId + ";";
		int DriverId = 0;
		try {
			stmt.executeUpdate(sqlCmd);
			rs = stmt.executeQuery(sqlCmd2);
			while (rs.next()) {
				driverId = Integer.parseInt(rs.getString(1));
			}
			if (driverId == DriverId) {
				res = "Successfully Archived DriverId: " + driverId;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			res = e.getMessage();
		}
		return res;
	}

	public String archiveDelDock(int delDockID) {
		String res = "";
		String sqlCmd = "delete from deliverydocket where DelDock_ID = " + delDockID + ";";
		String sqlCmd2 = "select * from deliverydocket_archive where DelDock_ID = " + delDockID + ";";
		int DelDockID = 0;
		try {
			stmt.executeUpdate(sqlCmd);
			rs = stmt.executeQuery(sqlCmd2);
			while (rs.next()) {
				delDockID = Integer.parseInt(rs.getString(1));
			}
			if (delDockID == DelDockID) {
				res = "Successfully Archived DeliveryDocket: " + delDockID;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			res = e.getMessage();
		}
		return res;
	}

	public String addPubToSub(int subID, int pubID) {
		String fKeyOff = "SET foreign_key_checks = 0;";
		String sqlCmd = "UPDATE Subscription SET pubID = " + pubID + " where subID = " + subID + ";";
		String fKeyOn = "SET foreign_key_checks = 1;";
		String res = "";
		try {
			stmt.executeUpdate(fKeyOff);
			stmt.executeUpdate(sqlCmd);
			stmt.executeUpdate(fKeyOn);
			res = "Publication ID " + pubID + " added to Subscription ID " + subID;
		} catch (SQLException e) {
			e.printStackTrace();
			res = e.getMessage();
		}
		return res;
	}


	public String addSubToDelDock(int delDockID, int subID) {
		String fKeyOff = "SET foreign_key_checks = 0;";
		String sqlCmd = "UPDATE DeliveryDocket SET sub_ID = " + subID + " where DelDock_ID = " + delDockID + ";";
		String sqlCmd2 = "UPDATE subscription SET DelDock_ID = " + delDockID + " where subID = " + subID + ";";
		String fKeyOn = "SET foreign_key_checks = 1;";
		String res = "";
		try {
			stmt.executeUpdate(fKeyOff);
			stmt.executeUpdate(sqlCmd);
			stmt.executeUpdate(sqlCmd2);
			stmt.executeUpdate(fKeyOn);
			res = "Subscription ID " + subID + " added to Delivery Docket ID " + delDockID;
		} catch (SQLException e) {
			e.printStackTrace();
			res = e.getMessage();
		}
		return res;
	}


	public String lookupDriverByName(String name) {
		String driverID = "";
		String cmd = "Select * from Driver where driverName = '" + name + "';";
		try {
			rs = stmt.executeQuery(cmd);
			while (rs.next()) {
				driverID = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return driverID;
	}

	public boolean modifyDriver(int driverId, String driverName, String password) {
		String cmd = ("UPDATE Driver SET driverName = '" + driverName + "', password = " + password
				+ " where driverId = " + driverId + ";");
		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (Exception e1) {
		}
		return false;
	}

	public boolean modifyCust(int custId, String name, String address, int age, int delAreaID) {
		String cmd = ("UPDATE Customer SET Cust_Name = '" + name + "', Cust_Address = '" + address + "', Cust_Age = "
				+ age + " where Cust_ID = " + custId + ";");		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return false;

	}

	public String lookUpSubID(int subID) {
		int custID = 0;
		int pubID = 0;
		int delDocketID = 0;
		String frequency = "";
		String delivered = "";
		String res = "";
		String sqlCmd = "Select * from Subscription where subID = '" + subID + "';";

		try {
			rs = stmt.executeQuery(sqlCmd);
			while (rs.next()) {
				custID = rs.getInt(2);
				pubID = rs.getInt(3);
				delDocketID = rs.getInt(4);
				frequency = rs.getString(5);
				delivered = rs.getString(6);
			}
			if (custID == 0) {
				return "Subscription  Not Found";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "CustomerID: " + custID + ", " + "PublicationID: " + pubID + ", " + "Delivery Docket ID: " + delDocketID
				+ ", " + "Frequency = " + frequency + ", " + "Delivered = " + delivered;

	}

	public String createInvoice(int custID) {
		String cmd1 = "select * from subscription where Cust_ID = " + custID + ";";
		ArrayList<Object> pubIdList = new ArrayList<>();
		ArrayList<Object> pubCost = new ArrayList<>();
		ArrayList<Object> freq = new ArrayList<>();
		try {
			rs = stmt.executeQuery(cmd1);
			while (rs.next()) {
				if (rs.getInt(6) == 1) {
					pubIdList.add(rs.getInt(3));
					freq.add(rs.getString(5));
				}
			}
			for (int i = 0; i < pubIdList.size(); i++) {
				String cmd2 = "select * from publication where pubID = " + pubIdList.get(i) + ";";
				rs = stmt.executeQuery(cmd1);
				while (rs.next()) {
					pubCost.add(rs.getInt(3));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	public String completeDeliveryDocket(int delDockID, boolean status) {
		String cmd = ("UPDATE deliverydocket SET delivered = " + status + " where DelDock_ID = " + delDockID + ";");
		try {
			stmt.executeUpdate(cmd);
			return "Delivery Docket Updated";
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return "Delivery Docket Not Updated";
	}

	public boolean modifyDelArea(int delAreaID, String areaLocation) {
		String cmd = ("UPDATE deliveryarea SET areaLocation = '" + areaLocation + "' where delAreaId = " + delAreaID
				+ ";");
		System.out.println(cmd);
		try {
			stmt.executeUpdate(cmd);
			return true;
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return false;
		
	}

	public boolean driverLogin(int driverId, String password) {
		String sqlCmd = "Select * from driver where DriverId = " + driverId + ";";
		int driverIdDB = 0;
		String passwordDB = "";
		try {
			rs = stmt.executeQuery(sqlCmd);
			while (rs.next()) {
				driverIdDB = rs.getInt(1);
				passwordDB = rs.getString(3);
				
			}if(driverId == driverIdDB && password.equals(passwordDB)) {
				return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
	}
		
	return false;

	}
}

