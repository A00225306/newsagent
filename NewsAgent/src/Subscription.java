
public class Subscription {

	public boolean createSubscritpion(String frequency, Database db) {
		boolean res = false;
		try {
			if (frequency.equals("Daily") || frequency.equals("Weekend") || frequency.equals("MtoF")) {
				if (db.createSubscritpion(frequency)) {
					res = true;
				}
			} else {
				throw new ExceptionHandler("Invalid Input");
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
		}
		return res; 
	}

	public String addPubToSub(int subID, int pubID, Database db) {
		String res = "";
		try {
			if (pubID < 1) {
				throw new ExceptionHandler("Invalid Publication ID");
			} else if (subID < 1) {
				throw new ExceptionHandler("Invalid Subscription ID");
			} else {
				res = db.addPubToSub(subID, pubID);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res;
	}

	public boolean modifySubscription(int subID, String Freq, boolean b, Database db) {
		boolean res = false;
		try {
			if (Freq.equals("Daily") || Freq.equals("Weekend") || Freq.equals("MtoF")){
				if (db.modifySub(subID, Freq, b)) {
					res = true;
				}
			} else {
				throw new ExceptionHandler("Invalid Frequency");
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
		}
		return res;
	}	
	
	public String archiveSub(int subID, Database db) {
		String res = "";
		try {
			if (subID < 1) {
				throw new ExceptionHandler("Invalid Subscription ID");
			} else if (db.lookUpSubID(subID).equals("Subscription  Not Found")) {
				res = "Subscription  Not Found";
			} else {
				res = db.archiveSub(subID);
			}
		} catch (ExceptionHandler e) {
			System.out.println(e.getMessage());
			res = e.getMessage();
		}
		return res;
	}
	public String subLookUp(int subID, Database db) {
		String res = "";
		try {
			if(subID < 1) {
				throw new ExceptionHandler("Invalid Subscription ID");
			}else {
				res = db.lookUpSubID(subID);
				//System.out.println(res);
			}
		}catch(ExceptionHandler e){
				System.out.println(e.getMessage());
				res = e.getMessage();
			}
		
		return res;
		
	}
	
}