CREATE DATABASE IF NOT EXISTS NewsAgent;
USE NewsAgent;

DROP TABLE IF EXISTS Subscription;
DROP TABLE IF EXISTS Publication;
DROP TABLE IF EXISTS Customer;
DROP TABLE IF EXISTS Driver;
DROP TABLE IF EXISTS driver_archive;
DROP TABLE IF EXISTS DeliveryDocket;
DROP TABLE IF EXISTS customer_archive;
DROP TABLE IF EXISTS deliverydocket_archive;
DROP TRIGGER IF EXISTS customer_after_delete;
DROP TABLE IF EXISTS DeliveryArea;
DROP TABLE IF EXISTS deliveryArea_archive;
DROP TABLE IF EXISTS publication_archive;
Drop trigger if exists sub_after_delete;
Drop trigger if exists deliverydocket_after_delete;
Drop trigger if exists driver_after_delete;
DROP TABLE IF EXISTS sub_after_delete;

CREATE TABLE Customer (
	Cust_ID int NOT NULL AUTO_INCREMENT,
	Cust_Name VARCHAR(50),
	Cust_Address VARCHAR(99),
	Cust_Age  int(3),
	Del_ID  int(4),
    PRIMARY KEY (Cust_ID)
	);

CREATE TABLE Driver (
	DriverId int NOT NULL AUTO_INCREMENT,
	driverName 	VARCHAR(50),
	password VARCHAR(50),
    PRIMARY KEY (DriverId)
	);
    
CREATE TABLE DeliveryArea (
	delAreaID int NOT NULL AUTO_INCREMENT,
	areaLocation VARCHAR(50),
    PRIMARY KEY (delAreaID)
	);
        
CREATE TABLE Publication(
	pubID int NOT NULL AUTO_INCREMENT,
	pubName VARCHAR(20),
	pubCost decimal(4,2),	
	PRIMARY KEY (pubID)
	);	
    
CREATE TABLE DeliveryDocket (
	DelDock_ID int NOT NULL AUTO_INCREMENT,
	driver_ID integer (4),
    sub_ID integer (4),
    delivered boolean,
    PRIMARY KEY (DelDock_ID)
	);

CREATE TABLE Subscription(
	subID int NOT NULL AUTO_INCREMENT, 
    Cust_ID int,
    pubID int,
    DelDock_ID int,
	subFreq VARCHAR(20),
    active boolean,
	PRIMARY KEY (subID),
    FOREIGN KEY (Cust_ID) REFERENCES Customer(Cust_ID),
    FOREIGN KEY (pubID) REFERENCES Publication(pubID),
    FOREIGN KEY (DelDock_ID) REFERENCES DeliveryDocket(DelDock_ID)
	);

CREATE TABLE customer_archive (
    Cust_ID int NOT NULL PRIMARY KEY,
	Cust_Name VARCHAR(50),
	Cust_Address VARCHAR(99),
	Cust_Age  int(3),
	Del_ID  int(4),
    Delete_Date DATETIME NOT NULL
);

CREATE TABLE deliverydocket_archive(
	DelDock_ID int NOT NULL PRIMARY KEY,
	driver_ID integer (4),
    delivered boolean,
	Delete_Date DATETIME NOT NULL
);

CREATE TABLE deliveryArea_archive(
	delAreaID int NOT NULL PRIMARY KEY,
   	areaLocation VARCHAR(50),
	Delete_Date DATETIME NOT NULL
);

CREATE TABLE publication_archive (
    pubID int NOT NULL PRIMARY KEY , 
	pubName VARCHAR(20),
	pubCost decimal(4,2),
    Delete_Date DATETIME NOT NULL
);

CREATE TABLE driver_archive (
    DriverId int NOT NULL PRIMARY KEY,
	driverName VARCHAR(50),
	password VARCHAR(99),
    Delete_Date DATETIME NOT NULL
);

CREATE TABLE  sub_after_delete(
	subID int,
    Cust_ID int,
    pubID int,
    DelDock_ID int,
	subFreq VARCHAR(20),
	Delete_Date DATETIME NOT NULL
	);

DELIMITER //
CREATE  TRIGGER sub_after_delete
	AFTER DELETE on subscription
	FOR EACH ROW
BEGIN
	INSERT INTO sub_after_delete VALUES
    (OLD.subID,OLD. Cust_ID, OLD.pubID, OLD.DelDock_ID,OLD.subFreq, NOW());   

END//


DELIMITER //
CREATE  TRIGGER driver_after_delete
    AFTER DELETE on Driver
    FOR EACH ROW
BEGIN
    INSERT INTO driver_archive VALUES
    (OLD.DriverId, OLD.driverName,OLD.password, NOW());   
END//

DELIMITER //
CREATE  TRIGGER customer_after_delete
    AFTER DELETE on customer
    FOR EACH ROW
BEGIN
    INSERT INTO customer_archive VALUES
    (OLD.Cust_ID, OLD.Cust_Name,OLD.Cust_Address,OLD.Cust_Age,OLD.Del_ID, NOW());   
END//
Drop trigger if exists deliverydocket_after_delete;

DELIMITER //
CREATE  TRIGGER deliverydocket_after_delete
	AFTER DELETE on deliverydocket
	FOR EACH ROW
BEGIN
	INSERT INTO deliverydocket_archive VALUES
    (OLD.DelDock_ID, OLD.driver_ID, OLD.delivered, NOW());   

END//

DELIMITER //

CREATE  TRIGGER delArea_after_delete
	AFTER DELETE on DeliveryArea
	FOR EACH ROW
BEGIN
	INSERT INTO deliveryArea_archive VALUES
	(OLD.delAreaID, OLD.areaLocation, NOW());   
END//

DELIMITER //
CREATE  TRIGGER publication_after_delete
    AFTER DELETE on publication
    FOR EACH ROW
BEGIN
    INSERT INTO publication_archive VALUES
    (OLD.pubID, OLD.pubName,OLD.pubCost, NOW());   
END//